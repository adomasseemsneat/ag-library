import { ThemeProvider } from "styled-components";
import { Route } from "react-router-dom";
import { Main, Welcome  } from "views";
import GlobalStyles from "./Global";

const theme = {
  colors: {
    header: 'blue',
    body: 'red',
    footer:'#003333'
  }
}


function App() {
  return (
    <ThemeProvider theme={theme} >
      <GlobalStyles/>
      <div className="App">
        <Route exact path="/" component={Main} />
        <Route exact path="/welcome" component={Welcome} />
      </div>
    </ThemeProvider>
  );
}

export default App;
