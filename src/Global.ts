import { createGlobalStyle } from "styled-components";



const fontfamilyArray = [
  "'Azeret Mono', monospace",
  "'Roboto', sans-serif;",
  "'Yellowtail', cursive;"
]


const GlobalStyles = createGlobalStyle` 
  @import url('https://fonts.googleapis.com/css2?family=Azeret+Mono:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&family=Yellowtail&display=swap');

  *{box-sizing: border-box}

  html{
    scroll-behavior: smooth;
    margin: 0;
    padding: 0;
  }

  body{
    margin: 0;
    padding: 0;
    font-size: 1.15em;
    font-family: ${fontfamilyArray[0]}
  }
  p{opacity: 0.6; line-height: 1.5;}

  img{max-width: 100%}




`

export default GlobalStyles