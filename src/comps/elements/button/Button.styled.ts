import React from 'react'
import styled from 'styled-components'

export const Button = styled.button`
  background: transparent;
  border-radius: 3px;
  border: 2px solid palevioletred;
  color: palevioletred;
  margin: 0 1em;
  padding: 0.25em 1em;
  text-decoration: none;
  &:hover{
    opacity: 0.9;
    transform: scale(0.98);
    box-shadow:
      0px 0px 0.2px rgba(0, 0, 0, 0.098),
      0px 0px 0.5px rgba(0, 0, 0, 0.141),
      0px 0px 0.9px rgba(0, 0, 0, 0.175),
      0px 0px 1.6px rgba(0, 0, 0, 0.209),
      0px 0px 2.9px rgba(0, 0, 0, 0.252),
      0px 0px 7px rgba(0, 0, 0, 0.35)
      ;

  }
  &>a{
    color: palevioletred;
    text-decoration: none;
  }
  

`
