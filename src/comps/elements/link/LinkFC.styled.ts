import styled from "styled-components"

// import styled, { css } from "styled-components";
type coll = string | null | undefined;
interface IViewProps {
  coll: coll;
}


// const themeCol = ({theme}) => theme.colors.header 


export const LinkFC__styled = styled.a<IViewProps>`
  color: #8f8f8f;
  text-decoration: none;
  
  border-bottom: 2px solid ${({coll}) => coll || 'white'};
  font-weight: 600;
  &:hover{
    background: ${({coll}) => coll || '#3fd25a'};
    color: white;
  }
`
