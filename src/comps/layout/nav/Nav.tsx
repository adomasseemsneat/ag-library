import { Button, ContainerSC } from "comps";
import React from "react";
import { Link } from "react-router-dom";

export const Nav = () => {
  return (
    <nav>
      <ContainerSC>
        <Button>
          <Link to="/">Main</Link>  
        </Button>
        <Button>
          <Link to="/welcome">Welcome</Link>
        </Button>
        <Button>
          random
        </Button>
      </ContainerSC>
    </nav>
  );
};
//
