
import { ContainerSC, Nav } from 'comps'
import React from 'react'
import { Link } from 'react-router-dom'

export const Main = () => {
  return (
    <div>
      <Nav></Nav>
      <ContainerSC>
        <h1>this is the main</h1>
      </ContainerSC>
    </div>
  )
}
