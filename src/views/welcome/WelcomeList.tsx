
import { LinkFC } from 'comps';
import React from 'react'

export const WelcomeList = () => {


  

  // * anchor tags
  const a = {
    tania: (
      <LinkFC col="red" href="https://www.taniarascia.com/react-architecture-directory-structure/" >
      tania
      </LinkFC>
    ),
    kasongoyo: (
      <LinkFC href="https://kasongoyo.medium.com/using-alias-with-react-webpack-and-typescript-72119dbc498">
        kasongoyo
      </LinkFC>
    ),
    maximilian: (
      <LinkFC href="https://www.udemy.com/course/react-the-complete-guide-incl-redux/learn/lecture/29303718#overview">
        maximilian Academind by Maximilian Schwarzmüller
      </LinkFC> 
    ),
    toolkit: <LinkFC href="https://redux-toolkit.js.org/">redux toolkit</LinkFC>,
    styled: <LinkFC href="https://styled-components.com/">styled components</LinkFC>,
  };





  return (
    <ul>
        <li>folder structure by {a.tania} </li>
        <li>
          use typescript alias paths and absolute paths from {a.kasongoyo}
        </li>
        <li>react router as explained by {a.maximilian} </li>
        <li>index files suggestion by {a.tania}</li>
        <li>redux store and {a.toolkit} </li>
        <li>{a.styled} </li>
    </ul>
  )
}
